<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'confirm' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/confirm',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'confirm',
                    ),
                ),
            ),
            'logini' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/logini',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'logini',
                    ),
                ),
            ),
            'signupi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/signupi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'signup',
                    ),
                ),
            ),
            'contacti' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/contacti',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'contact',
                    ),
                ),
            ),
            'touri' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/touri',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'tour',
                    ),
                ),
            ),
            'homei' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/homei',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'home',
                    ),
                ),
            ),
            'mapi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/mapi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'map',
                    ),
                ),
            ),
            'groupsi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/groupsi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'groups',
                    ),
                ),
            ),
            'devicesi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/devicesi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'devices',
                    ),
                ),
            ),
            'managementi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/managementi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'management',
                    ),
                ),
            ),
            'usdetaili' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/usdetaili',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'userdetail',
                    ),
                ),
            ),
            'profilei' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/profilei',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'profile',
                    ),
                ),
            ),
            'forgoti' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/forgoti',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'forgot',
                    ),
                ),
            ),
            'reseti' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/reseti',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'reset',
                    ),
                ),
            ),
            'activatei' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/activatei',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'activate',
                    ),
                ),
            ),
            'termsi' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/termsi',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'terms',
                    ),
                ),
            ),
            'detaili' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/detaili',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'detail',
                    ),
                ),
            ),
            'grdetaili' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/grdetaili',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'grdetail',
                    ),
                ),
            ),
            'settimezone' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/settimezone',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'settimezone',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
