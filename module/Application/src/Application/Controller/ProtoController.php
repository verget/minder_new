<?php
namespace Application\Controller;

use Zend\View\Model\JsonModel;

abstract class ProtoController extends \Zend\Mvc\Controller\AbstractActionController
{
    public $isAjax = false; 
    
    public function onDispatch(\Zend\Mvc\MvcEvent $e){
        
        $isAjax = false;
        // If we have application/json request need set params from input
        if( $e->getRequest()->getHeaders('Content-Type') &&
            strpos($e->getRequest()->getHeaders('Content-Type')->getFieldValue(), 'application/json') !== false )
        {
            $data = json_decode(trim(file_get_contents("php://input")), true);
            $e->getRequest()->setPost( new \Zend\Stdlib\Parameters($data) );
            $isAjax = true;
        }
        
        if( !$isAjax && ( ( !$e->getRequest() instanceof \Zend\Console\Request && 
                        $e->getRequest()->isXmlHttpRequest() ) || 
            ( $e->getRequest()->getHeaders('Accept') && 
                    strpos($e->getRequest()->getHeaders('Accept')->getFieldValue(), 'application/json') !== false) ) )
            $isAjax = true;
        
        if( $isAjax ){
            $this->layout()->setTerminal(true);
            $this->layout('layout/ajax');
            $this->isAjax = true;
        }
        return parent::onDispatch($e);
    }
    
    public function sendJson( $data = true, $responseCode = \Zend\Http\Response::STATUS_CODE_200 )
    {
        
        $this->getResponse()->getHeaders()
                            ->addHeaderLine('Content-Type', 'application/json')
                            ->addHeaderLine('Pragma', 'no-cache')
                            ->addHeaderLine('X-Content-Type-Options', 'nosniff')
                            ->addHeaderLine('Vary', 'Accept');
        $this->getResponse()->setStatusCode($responseCode);
        $this->layout()->setTerminal(true);
        $this->layout('layout/ajax');
       
        $model = new JsonModel(['data' => json_encode($data)]);
        $model->setTemplate('application/index/ajax.phtml');
        return $model;
    }
    
    /**
     * Return Authanticated User
     */
    public function getUser(){
//         return \Pos\Model\PosUsers::getAuthUser();
    }
}