<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hrayr
 * Date: 10/16/13
 * Time: 7:50 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;
use Zend\Session\Container;
use Zend\Math\Rand;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;



class MinderAbstractController extends AbstractActionController
{
    protected $agentsTable;
    protected $authTable;
    protected $paymentTable;
    protected $licenseTable;
    protected $agentMenteeTable;
    protected $groupTable;
    protected $pdfClassTable;
    protected $userInformationTable;
    protected $userGroupTable;
    protected $agentGroupTable;
    protected $instructorUsersTable;
    protected $groupMenteeTable;
    protected $menteeParamsTable;
    protected $menteeProgramTable;
    protected $menteeTransactionTable;
    protected $general;
    protected $storage;
    protected $agentsService;


    public function isAgent()
    {
        if (!$this->getAgentsService()->hasIdentity()) {
            $this->flashMessenger()->addErrorMessage("Please login at first!");
            header("Location: /agent/login");
            die();
        } else {
            return true;
        }
    }
    public function getPdfClassTable()
    {
        if (!$this->pdfClassTable) {
            $sm = $this->getServiceLocator();
            $this->pdfClassTable = $sm->get('Agents\Model\PdfClassTable');
        }
        return $this->pdfClassTable;
    }

    public function getAgentsTable()
    {
        if (!$this->agentsTable) {
            $sm = $this->getServiceLocator();
            $this->agentsTable = $sm->get('Agents\Model\AgentsTable');
        }
        return $this->agentsTable;
    }

    public function getPaymentTable()
    {
        if (!$this->paymentTable) {
            $sm = $this->getServiceLocator();
            $this->paymentTable = $sm->get('Auth\Model\PaymentTable');
        }
        return $this->paymentTable;
    }

    public function getUserInformationTable()
    {
        if (!$this->userInformationTable) {
            $sm = $this->getServiceLocator();
            $this->userInformationTable = $sm->get('Auth\Model\UserInformationTable');
        }
        return $this->userInformationTable;
    }

    public function getLicenseTable()
    {
        if (!$this->licenseTable) {
            $sm = $this->getServiceLocator();
            $this->licenseTable = $sm->get('Agents\Model\ReLicenseTable');
        }
        return $this->licenseTable;
    }

    public function getInstructorUsersTable()
    {
        if (!$this->instructorUsersTable) {
            $sm = $this->getServiceLocator();
            $this->instructorUsersTable = $sm->get('Agents\Model\InstructorUsersTable');
        }
        return $this->instructorUsersTable;
    }

    public function generate($length = 12)
    {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM', false);
    }

    public function getGeneralTable()
    {
        if (!$this->general) {
            $sm = $this->getServiceLocator();
            $this->general = $sm->get('Application\Model\General');
        }
        return $this->general;
    }

    public function getAgentsService()
    {
        if (!$this->agentsService) {
            $this->agentsService = $this->getServiceLocator()
                ->get('AgentsService');
        }

        return $this->agentsService;
    }

    public function getSessionStorage()
    {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('Agents\Model\AgentsStorage');
        }
        return $this->storage;
    }

    public function getAuthTable()
    {
        if (!$this->authTable) {
            $sm = $this->getServiceLocator();
            $this->authTable = $sm->get('Auth\Model\AuthTable');
        }
        return $this->authTable;
    }

    public function getAgentMenteeTable()
    {
        if (!$this->agentMenteeTable) {
            $sm = $this->getServiceLocator();
            $this->agentMenteeTable = $sm->get('Mentee\Model\AgentMenteeTable');
        }
        return $this->agentMenteeTable;
    }

    public function getGroupTable()
    {
        if (!$this->groupTable) {
            $sm = $this->getServiceLocator();
            $this->groupTable = $sm->get('Mentee\Model\GroupTable');
        }
        return $this->groupTable;
    }

    public function getGroupMenteeTable()
    {
        if (!$this->groupMenteeTable) {
            $sm = $this->getServiceLocator();
            $this->groupMenteeTable = $sm->get('Mentee\Model\GroupMenteeTable');
        }
        return $this->groupMenteeTable;
    }

    public function getMenteeParamsTable()
    {
        if (!$this->menteeParamsTable) {
            $sm = $this->getServiceLocator();
            $this->menteeParamsTable = $sm->get('Mentee\Model\MenteeParamsTable');
        }
        return $this->menteeParamsTable;
    }

    public function getMenteeTransactionTable()
    {
        if (!$this->menteeTransactionTable) {
            $sm = $this->getServiceLocator();
            $this->menteeTransactionTable = $sm->get('Mentee\Model\MenteeTransactionTable');
        }
        return $this->menteeTransactionTable;
    }

    public function getMenteeProgramTable()
    {
        if (!$this->menteeProgramTable) {
            $sm = $this->getServiceLocator();
            $this->menteeProgramTable = $sm->get('Mentee\Model\MenteeProgramTable');
        }
        return $this->menteeProgramTable;
    }

    public function getUsersGroupTable()
    {
        if (!$this->userGroupTable) {
            $sm = $this->getServiceLocator();
            $this->userGroupTable = $sm->get('Mentee\Model\UsersGroupTable');
        }
        return $this->userGroupTable;
    }

    public function getAgentsGroupTable()
    {
        if (!$this->agentGroupTable) {
            $sm = $this->getServiceLocator();
            $this->agentGroupTable = $sm->get('Mentee\Model\AgentsGroupTable');
        }
        return $this->agentGroupTable;
    }

    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authService;
    }

    public function getUserSessionStorage()
    {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('Auth\Model\AuthStorage');
        }
        return $this->storage;
    }


    public function sendMail($htmlBody, $textBody,$subject, $from, $to, $cc, $bcc)
    {
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html; charset=UTF-8";

        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain; charset=UTF-8";
        $body = new MimeMessage();
        $body->setParts(array($textPart, $htmlPart));

        $message = new Mail\Message();
        $message->setFrom($from)
            ->addTo($to)
            ->addCc($cc)
            ->addBcc($bcc)
            ->setSubject($subject);

        $message->setEncoding("UTF-8");
        $message->setBody($body);

        $transport = new SendmailTransport();
        $transport->send($message);
    }

    public function sendMailAttachments($htmlBody, $textBody,$attachments=false, $subject, $from, $to, $cc, $bcc,$username)
    {
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html; charset=UTF-8";

        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain; charset=UTF-8";
        $attachmentsArray=array();
         if($attachments){
            foreach($attachments as $fileName){
            $attachment = new MimePart(fopen(dirname(__DIR__) . '/../../../../public/classes/' . $username . '/'.$fileName, 'r'));
            $attachment->type = 'application/pdf';
            $attachment->filename=$fileName;
            $attachment->encoding    = Mime\Mime::ENCODING_BASE64;
            $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
                $attachmentsArray[]=$attachment;
            }
        }
        $mailBody=array_merge(array($textPart, $htmlPart),$attachmentsArray);
        $body = new MimeMessage();
        $body->setParts($mailBody);

        $message = new Mail\Message();
        $message->setFrom($from)
            ->addTo($to)
            ->addCc($cc)
            ->addBcc($bcc)
            ->setSubject($subject);

        $message->setEncoding("UTF-8");
        $message->setBody($body);

        $transport = new SendmailTransport();
        $transport->send($message);
    }

    public function sendLicenseEmail($htmlBody, $textBody,$img='', $subject, $from, $to, $cc, $bcc,$username)
    {
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html; charset=UTF-8";

        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain; charset=UTF-8";
        $attachmentsArray=array();
            $attachment = new MimePart(fopen($img, 'r'));
            $attachment->type = 'application/pdf';
            $attachment->filename='License';
            $attachment->encoding    = Mime\Mime::ENCODING_BASE64;
            $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
                $attachmentsArray[]=$attachment;
        $mailBody=array_merge(array($textPart, $htmlPart),$attachmentsArray);
        $body = new MimeMessage();
        $body->setParts($mailBody);

        $message = new Mail\Message();
        $message->setFrom($from)
            ->addTo($to)
            ->addCc($cc)
            ->addBcc($bcc)
            ->setSubject($subject);

        $message->setEncoding("UTF-8");
        $message->setBody($body);

        $transport = new SendmailTransport();
        $transport->send($message);
    }

    public function sendInstructorClass($htmlBody, $textBody,$img='', $subject, $from, $to, $cc, $bcc,$username)
    {
        $htmlPart = new MimePart($htmlBody);
        $htmlPart->type = "text/html; charset=UTF-8";

        $textPart = new MimePart($textBody);
        $textPart->type = "text/plain; charset=UTF-8";
        $attachmentsArray=array();
            $attachment = new MimePart(fopen($img, 'r'));
            $attachment->type = 'application/pdf';
            $attachment->filename='Class';
            $attachment->encoding    = Mime\Mime::ENCODING_BASE64;
            $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;
                $attachmentsArray[]=$attachment;
        $mailBody=array_merge(array($textPart, $htmlPart),$attachmentsArray);
        $body = new MimeMessage();
        $body->setParts($mailBody);

        $message = new Mail\Message();
        $message->setFrom($from)
            ->addTo($to)
            ->addCc($cc)
            ->addBcc($bcc)
            ->setSubject($subject);

        $message->setEncoding("UTF-8");
        $message->setBody($body);

        $transport = new SendmailTransport();
        $transport->send($message);
    }

    public function logoutAgent()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAgentsService()->clearIdentity();
    }

    public function loginAgent($agent)
    {
        $this->getSessionStorage()->setRememberMe(1);

        $this->getAgentsService()->setStorage($this->getSessionStorage());

        $this->getAgentsService()->getStorage()->write($agent);
    }

    public function reLoginAgent() {
        $agent=$this->getServiceLocator()->get('AgentsService')->getStorage()->read();
        $agentNew=$this->getAgentsTable()->getById($agent->getId());
        $this->logoutAgent();
        $this->loginAgent($agentNew);
    }

    public function logoutUser()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
    }

    public function loginUser($agent)
    {
        $this->getSessionStorage()->setRememberMe(1);

        $this->getAuthService()->setStorage($this->getSessionStorage());

        $this->getAuthService()->getStorage()->write($agent);
    }

    public function reLoginUser() {
        $agent=$this->getServiceLocator()->get('AuthService')->getStorage()->read();
        $agentNew=$this->getAuthTable()->getById($agent->getId());
        $this->logoutUser();
        $this->loginUser($agentNew);
    }

    public function getAgent(){
        $agent=$this->getAgentsService()->getStorage()->read();
        return $agent;
    }

    public function hasIdentityAgent(){
        if (!$this->getAgentsService()->hasIdentity()) {
            return $this->redirect()->toUrl('/agent/login');
        }
    }

    public function getStatesList()
    {
        $states=$this->getAgentsTable()->getStates();

        $data = array();
        foreach ($states as $state) {
            $data[$state['code']] = $state['name'];
        }
        return $data;
    }

    public function getAgentsCount()
    {
        $agents=$this->getAgentsTable()->getAgentsCount();

        return $agents;
    }

    public function getClientsCount()
    {
        $clients= $this->getAuthTable()->getClientsCount();

        return $clients;
    }

    /**
     * @return array
     * Get all classes from 2010 to current year
     */

    public function getClasses()
    {
        $this->isAgent();

        $currentYear = date('Y');
        $agent = $this->getAgentsService()->getStorage()->read();
        $agentId = $agent->getId();
        $data = array();
        $hours = 0;
        for ($currentYear; $currentYear >= 2010; $currentYear--) {
            $classes = $this->getServiceLocator()->get('Agents\Model\PdfClassTable')->fetchByYear($currentYear, $agentId);
            foreach ($classes as $c) {
                $data[$currentYear][] = $c;
                $hours += $c['hours'];
            }
            if ($hours != 0)
                $data[$currentYear]['total_hours'] = $hours;
            $hours = 0;
        }
        return $data;
    }

    public function getClassesById($id)
    {
        $currentYear = date('Y');
        $data = array();
        $hours = 0;
        for ($currentYear; $currentYear >= 2010; $currentYear--) {
            $classes = $this->getServiceLocator()->get('Agents\Model\PdfClassTable')->fetchByYear($currentYear, $id);
            foreach ($classes as $c) {
                $data[$currentYear][] = $c;
                $hours += $c['hours'];
            }
            if ($hours != 0)
                $data[$currentYear]['total_hours'] = $hours;
            $hours = 0;
        }
        return $data;
    }

    public function getInstructorClasses()
    {
        $this->isAgent();

        $currentYear = date('Y');
        $agent = $this->getAgentsService()->getStorage()->read();
        $agentId = $agent->getId();
        $data = array();
        $hours = 0;
        for ($currentYear; $currentYear >= 2010; $currentYear--) {
            $classes = $this->getServiceLocator()->get('Agents\Model\PdfClassTable')->fetchByYearAndInstructor($currentYear, $agentId);
            foreach ($classes as $c) {
                $data[$currentYear][] = $c;
                $hours += $c['hours'];
            }
            if ($hours != 0)
                $data[$currentYear]['total_hours'] = $hours;
            $hours = 0;
        }
        return $data;
    }

    /**
     * @return array
     *
     * Get all mandatory classes from 2010 to current year
     */
    public function getMandatoryClasses()
    {
        $this->isAgent();

        $currentYear = date('Y');
        $agent = $this->getAgentsService()->getStorage()->read();
        $agentId = $agent->getId();
        $mandatoryData = array();
        for ($currentYear; $currentYear >= 2010; $currentYear--) {
            $mandatory = $this->getServiceLocator()->get('Agents\Model\PdfClassTable')->fetchMandatoryClass($currentYear, $agentId);
            $mandatoryData[$currentYear] = $mandatory;
        }
        return $mandatoryData;
    }

    public function getMandatoryClassesById($id)
    {
        $currentYear = date('Y');
        $mandatoryData = array();
        for ($currentYear; $currentYear >= 2010; $currentYear--) {
            $mandatory = $this->getServiceLocator()->get('Agents\Model\PdfClassTable')->fetchMandatoryClass($currentYear, $id);
            $mandatoryData[$currentYear] = $mandatory;
        }
        return $mandatoryData;
    }

}