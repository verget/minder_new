<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $timezone = array_key_exists('time', $_SESSION) ? $_SESSION['time'] : "";

        return new ViewModel(array(
            "loggedIn" => $this->isLoggedIn(),
            "timezone" => $timezone,
        ));
    }

    public function homeAction()
    {
        return new ViewModel(array(
            "loggedIn" => $this->isLoggedIn(),
        ));
    }

    public function mapAction()
    {
        return new ViewModel(array(
            "loggedIn" => $this->isLoggedIn(),
        ));
    }

    public function confirmAction()
    {
        return new ViewModel();
    }

    public function loginiAction()
    {
        return new ViewModel();
    }

    public function signupAction()
    {
        return new ViewModel();
    }

    public function contactAction()
    {
        return new ViewModel();
    }

    public function tourAction()
    {
        return new ViewModel();
    }

    public function groupsAction()
    {
        return new ViewModel();
    }

    public function devicesAction()
    {
        return new ViewModel();
    }

    public function managementAction()
    {
        return new ViewModel();
    }

    public function userdetailAction()
    {
        return new ViewModel();
    }

    public function profileAction()
    {
        return new ViewModel();
    }

    public function forgotAction()
    {
        return new ViewModel();
    }

    public function resetAction()
    {
        return new ViewModel();
    }

    public function activateAction()
    {
        return new ViewModel();
    }

    public function termsAction()
    {
        return new ViewModel();
    }

    public function detailAction()
    {
        return new ViewModel();
    }

    public function grdetailAction()
    {
        return new ViewModel();
    }

    public function settimezoneAction()
    {
        $_SESSION['time'] = -($_GET['time']);
        echo "done";
        exit;
    }

    private function isLoggedIn()
    {
        $loggedIn = false;
        if (isset($_COOKIE['loggedin']) && strpos($_COOKIE['loggedin'], "true")) {
            $loggedIn = true;
        }
        return $loggedIn;
    }
}
