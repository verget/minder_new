<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController'
//             'User\Controller\Management' => 'User\Controller\ManagementController',
//             'User\Controller\UserDetail' => 'User\Controller\UserDetailController',
//             'User\Controller\Activate' => 'User\Controller\ActivateController',
//             'User\Controller\Reset' => 'User\Controller\ResetController',
//             'User\Controller\Auth' => 'User\Controller\AuthenticationController',
//             'User\Controller\Forgot' => 'User\Controller\ForgotController',
//             'User\Controller\Login' => 'User\Controller\LoginController',
//             'User\Controller\Contact' => 'User\Controller\ContactController',
//             'UserDevice\Controller\UserDevice' => 'UserDevice\Controller\UserDeviceController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/:action[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'index',
                    ),
                ),
            ),
         ),
      ),


        'view_manager' => array(
            'template_path_stack' => array(
                'user' => __DIR__ . '/../view',
            ),
            'strategies' => array(
                'ViewJsonStrategy',
            ),
        ),
    'module_layouts' => array(
        'User' => 'layout/user',
    ),
    );