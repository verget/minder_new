<?php
/**
 * Created by JetBrains PhpStorm.
 * User: hrayr
 * Date: 2/26/13
 * Time: 6:10 PM
 * To change this template use File | Settings | File Templates.
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Math\Rand;
use Zend\Db\Adapter\Adapter;
use User\Model\User;
use Zend\View\Model\JsonModel;
use Zend\Crypt\Password\Bcrypt;

class UserTable
{
    protected $tableGateway;
    protected $_primary = "id";
    protected $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = new Adapter($this->tableGateway->getAdapter()->getDriver());
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select()->buffer();
        return $resultSet;
    }

    private function generate($length = 32) {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM', false);
    }

    public function register(array $user)
    {
        $date = date('Y-m-d H:i:s');
        $bcrypt = new Bcrypt();
        $password = $bcrypt->create($user['password']);
        $data=array(
            'first_name'=>$user['first_name'],
            'last_name'=>$user['last_name'],
            'email'=>$user['email'],
            'password'=>$password,
            'created_at'=>$date,
            'updated_at'=>$date,
            'minder_id'=>$user['minder_id'],
            'status'=>0,
            'hash'=>$user['hash'],
            'forgot_hash'=>$user['forgot_hash'],
            'type'=>'user'

        );

            $this->tableGateway->insert($data);



        return $this->tableGateway->lastInsertValue;
    }

    public function update($data,$id)
    {
        $this->tableGateway->update($data, array('id' => $id));
    }

    public function changePassword($password,$id)
    {
        $salt=$this->generate();
        $password=md5($password.$salt).':'.$salt;

        $this->tableGateway->update(array('password'=>$password), array('id' => $id));
    }

    public function activate($id)
    {
        $this->tableGateway->update(array('status' => 1), array('id' => $id));
    }

    public function resetPassword($password, $hash)
    {
        $this->tableGateway->update(array('password' => $password), array('forgot_hash' => $hash));
    }

    public function fetchByActivationLink($key)
    {
        $resultSet = $this->tableGateway->select(array('activation_code' => $key, 'user_status' => 'disable'));
        $column = $resultSet->current();
        return $column;
    }

    public function fetchByEmail($email)
    {
        $resultSet = $this->tableGateway->select(array('email'=>$email));
        $column=$resultSet->current();
        return $column;
    }
    public function selectMinderId()
    {
        $resultSet = $this->tableGateway->select();
        $data=array();
        foreach($resultSet as $value){
            $data[] = (array) $value;
        }
        return $data;

    }
    public function getCount()
    {
        $counts=$this->adapter->query("select count(*) as cnt, user_id from user_device group by user_id")->execute();
        $data=array();
        foreach($counts as $value){

            $data[$value["user_id"]] =$value["cnt"];

        }
        return $data;

    }

    public function fetchByHash($hash)
    {
        $resultSet = $this->tableGateway->select(array('hash'=>$hash));
        $column=$resultSet->current();
        return $column;
    }
    public function fetchByForgotHash($hash)
    {
        $resultSet = $this->tableGateway->select(array('forgot_hash'=>$hash));
        $column=$resultSet->current();
        return $column;
    }

    public function fetchByMinderId($minderId)
    {
        $resultSet = $this->tableGateway->select(array('minder_id'=>$minderId));
        $column=$resultSet->current();
        return $column;
    }

    public function fetchById($id)
    {
        $resultSet = $this->tableGateway->select(array('id'=>$id));
        $column=$resultSet->current();
        return $column;
    }

      public function save(User $obj)
    {
        $data = $obj->getArrayCopy();
        $id = (int) $obj->getId();
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, array($this->_primary => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function getById($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array($this->_primary => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array("id"=>$id));
    }

    public function getStates()
    {
        $states=$this->adapter->query("select * from state")->execute();

        return $states;
    }

    public function getForGrid(array $args, $status, $search = false, $order = false, $page = 1, $count = 10)
    {
        $where = "";
        if ($status != "0") {
            $status = ($status == 1 ? 2 : 1);
            $where .= " users.status = '" . $status . "'";
        }

        if ($search) {
            if ($where) {
                $where .= " AND ";
            }
            $where .= " concat_ws(' ', first_name, last_name) like '%" . $search . "%' ";
        }

        if (is_array($order)) {
            $order = " ORDER BY " . $order['orderField'] . " " . $order['orderType'];
        }

        $limit = " limit " . (($page - 1) * $count) . ", " . $count;

        if ($where) {
            $where = " WHERE " . $where;
        }

        $resultSet = $this->adapter->query("SELECT *, (select count(*) from user_device where user_device.user_id = users.id) as count FROM `users` " . $where . " " . $order . " " . $limit)->execute();
        $data = array();

        foreach ($resultSet as $value) {
            $data[] = $value;
        }

        return $data;
    }

    public function getCountForGrid(array $args, $status, $search = false)
    {
        $where = "";
        if ($status != "0") {
            $status = ($status == 1 ? 2 : 1);
            $where .= " users.status = '" . $status . "'";
        }

        if ($search) {
            if ($where) {
                $where .= " AND ";
            }
            $where .= " concat_ws(' ', first_name, last_name) like '%" . $search . "%' ";
        }

        if ($where) {
            $where = " WHERE " . $where;
        }

        $resultSet = $this->adapter->query("SELECT count(*) as c FROM `users` " . $where)->execute();
        $data = $resultSet->current();
        return $data['c'];
    }
}

