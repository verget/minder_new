<?php

/**
 * Class UserController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;
use Zend\Math\Rand;


class ActivateController extends AbstractRestfulController
{

    /**
     * Get User Service
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
        //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Logout action
     *
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        //return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     *
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

       /**
     * Singup action
     *
     *
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {
        $result = $this->getUserTable()->fetchByHash($data['hash']);
        if(!$result){
            return new JsonModel(array('status_code'=>400,'message'=>'Wrong actvation code!'));
        }

        if($result->getStatus()==0){
            $this->getUserTable()->activate($result->getId());
            return new JsonModel(array('status_code'=>200,'message'=>'Your account activated!'));
        } else {
            return new JsonModel(array('status_code'=>400,'message'=>'Your account has already activated!'));
        }

    }

    /**
     * Activate user action
     *
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {


    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    public function generate($length = 8)
    {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM', false);
    }
}
