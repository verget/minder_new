<?php

/**
 * Class UserController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mime;
use Zend\Math\Rand;
use Zend\Crypt\Password\Bcrypt;


class ResetController extends AbstractRestfulController
{

    /**
     * Get User Service
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
        //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Logout action
     *
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        //return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     *
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

       /**
     * Singup action
     *
     *
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {

        return new JsonModel(array('reset'));
    }

    /**
     * Activate user action
     *
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id,$data)
    {
        $hash=$data['hash'];
        $password=$data['password'];
        $confirm_password=$data['confirm_password'];
        $flo=array();
        $flo['password']=$password;
        $flo['confirm_password']=$confirm_password;
        $user = new User();
        $filter = $user->getInputFilterForReset();
        $filter->setData($flo);
        if ($filter->isValid()){

            $result = $this->getUserTable()->fetchByForgotHash($hash);
            if(!$result){
                return new JsonModel(array('status_code'=>400,'done'=>'Invalid reset password URL!'));
            }
            else{
                $bcrypt = new Bcrypt();
                $new_password = $bcrypt->create($password);
                $this->getUserTable()->resetPassword($new_password, $hash);
                return new JsonModel(array('status_code'=>200,'done'=>'password was successfully changed'));
            }

            return new JsonModel(array('reset'));
        }
        else{
            return new JsonModel(array('status_code' => 402, 'message' => $filter->getMessages()));
        }

    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    public function generate($length = 8)
    {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnmQWERTYUPASDFGHJKLZXCVBNM', false);
    }
}
