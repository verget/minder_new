<?php

/**
 * Class LoginController
 *
 * @package User
 * @copyright: coeus-solutions.de
 * @version 1.0
 * @author Coeus Solutions
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Crypt\Password\Bcrypt;

class LoginController extends AbstractRestfulController
{

    /**
     * Get user table
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Get device table
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\DeviceTable
     */
    public function getDeviceTable()
    {
        return $this->getServiceLocator()->get('Device\Model\DeviceTable');
    }

    /**
     * Get user device table
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserDeviceTable
     */
    public function getUserDeviceTable()
    {
        return $this->getServiceLocator()->get('UserDevice\Model\UserDeviceTable');
    }

    /**
     * Logout action
     *
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     *
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Login action
     *
     *
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {
        $userTable = $this->getUserTable();
        $userDeviceTable = $this->getUserDeviceTable();
        $user = $userTable->fetchByMinderId($data['minder_id']);

        $class = __CLASS__;
        $msg = var_export($data, true);
        error_log(Date("r") . " .$class . $msg");


        if (!$user) {
            return new JsonModel(array('status_code' => 400, 'message' => 'MinderWeb ID ' . $data['minder_id'] . ' does not exist. Do you want to sign up now?'));
        } else {
            if ($user->getStatus() != 2) {
                return new JsonModel(array('status_code' => 401, 'message' => 'Please activate account from email and then login with MinderWeb ID ' . $data['minder_id']));
            }

            $deviceExist = $this->getDeviceTable()->fetchByUdid($data['udid']);

            if ($deviceExist) {
                $rel = $userDeviceTable->fetchByUserAndDevice($user->getId(), $deviceExist->getId());
                if ($rel && $rel->getStatus() != "active") {
                    return new JsonModel(array('status_code' => 401, 'message' => 'Minder ID is not active. Please contact with administrator'));
                }
                $device['device_type'] = $data['device_type'];
                $device['os_version'] = $data['os_version'];
                $device['last_location'] = $data['last_location'];
                $device['last_request_status'] = "received";
                $device['updated_at'] = date('Y-m-d H:i:s');
                $this->getDeviceTable()->update($device, $deviceExist->getId());
                $deviceId = $deviceExist->getId();
                
                $this->getUserDeviceTable()->save(array(
                    'device_id' => $deviceId, 
                    'user_id' => $user->getId(),
                    'device_name' => $data['device_name'],
                    'device_status' => 'active'
                ));
            } else {
                $device = array();
                $device['udid'] = $data['udid'];
                $device['device_type'] = $data['device_type'];
                $device['os_version'] = $data['os_version'];
                $device['last_request_status'] = "received";
                $device['last_location'] = $data['last_location'];
                $device['created_at'] = date('Y-m-d H:i:s');
                $device['updated_at'] = date('Y-m-d H:i:s');
                $deviceId = $this->getDeviceTable()->save($device);

                $this->getUserDeviceTable()->save(array(
                    'device_id' => $deviceId, 
                    'user_id' => $user->getId(), 
                    'device_name' => $data['device_name'],
                    'device_status' => 'inactive'
                ));
            }

            return new JsonModel(array('status_code' => 200, 'message' => '', 'device_id' => $deviceId, 'user_id' => $user->getId()));
        }

    }

    /**
     * Activate user action
     *
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {

    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

}
