<?php

/**
 * Class ManagementController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;
use Zend\Math\Rand;
use Zend\Crypt\Password\Bcrypt;


class ManagementController extends AbstractRestfulController
{

    /**
     * Get User Service
     * 
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
      //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    public function getUserDeviceTable()
    {
        return $this->getServiceLocator()->get('UserDevice\Model\UserDeviceTable');
    }

    /**
     * Logout action
     * 
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        return new JsonModel(array('NOT IMPLEMENTED'));

    }

    /**
     * Get id
     * 
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Singup action
     * 
     * 
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {

        return new JsonModel(array('NOT IMPLEMENTED'));

    }

    /**
     * Activate user action
     * 
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data 
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {

        $user = new User();
        $filter = $user->getInputFilterForManagement();
        $filter->setData($data);
        if ($filter->isValid()) {
            if($data['password']){
                $bcrypt = new Bcrypt();
                $password = $bcrypt->create($data['password']);
                $data['password']=$password;
                unset($data['confirm_password']);
            }
            else{
                unset($data['password'],$data['confirm_password']);
            }
            $userTable=$this->getUserTable();
            $userTable->update($data,$id);
            return new JsonModel(array('status_code' => 200, 'message' => 'save'));

        }

        return new JsonModel(array('status_code' => 400, 'message' => $filter->getMessages()));
    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }


}
