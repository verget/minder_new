<?php

/**
 * Class UserController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;
use Zend\Math\Rand;


class UserController extends AbstractRestfulController
{

    /**
     * Get User Service
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
        //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    public function getUserDeviceTable()
    {
        return $this->getServiceLocator()->get('UserDevice\Model\UserDeviceTable');
    }

    /**
     * Logout action
     *
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {

        $userTable = $this->getUserTable();
        $users = $userTable->selectMinderId();
        $count = $userTable->getCount();
        $data = array();
        $allUsers = array();

        foreach ($users as $value) {

            if ($value['status'] == 2) {
                $data[] = $value;
            }
            $value['of_devices'] = 0;
            $user_id = $value['id'];
            foreach ($count as $key => $val) {
                if ($user_id == $key) {
                    $value['of_devices'] = $val;
                }
            }

            $allUsers[] = $value;

        }
        return new JsonModel(array('data' => $data, 'allUsers' => $allUsers));
    }

    /**
     * Get id
     *
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Singup action
     *
     *
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {
        if (array_key_exists('page', $data)) {
            $args = array();

            $users = $this->getUserTable()->getForGrid($args, $data['status'], $data['search'], array("orderType" => $data['orderType'], "orderField" => $data['orderField']), $data['page'], $data['count']);
            $rowsCount = $this->getUserTable()->getCountForGrid($args, $data['status'], $data['search']);
            return new JsonModel(array(
                'users' => $users,
                'quantity' => $rowsCount,
            ));
        } else {
            require_once(__DIR__ . "/../../../../Application/src/Application/Lib/recaptchalib.php");

            $privatekey = "6LfNUPgSAAAAALwspwBQV4S69S51UPE_CvcurdZ0"; //live
    //        $privatekey = "6LfOUPgSAAAAAEs3EBRp_xRg69Dv2LZMdmPU-8V_"; //local
            $inputJSON = file_get_contents('php://input');
            $input = json_decode( $inputJSON, TRUE );
            
            // If request not from mobile app
            if( $input['captcha']["challenge"] ){
                $resp = recaptcha_check_answer($privatekey,
                    $_SERVER["REMOTE_ADDR"],
                    $input['captcha']["challenge"],
                    $input['captcha']["response"]);
    
                if (!$resp->is_valid) {
                    // What happens when the CAPTCHA was entered incorrectly
                    return new JsonModel(array('status_code' => 402, 'agreement' => 'The reCAPTCHA wasn\'t entered correctly. Go back and try it again.'));
                }
            }

            $user = new User();
            $filter = $user->getInputFilterForSingup();
            $filter->setData($data);
            if ($filter->isValid()) {

                $userTable = $this->getUserTable();
                $currentUser = $userTable->fetchByEmail($data['email']);
                if ($currentUser != false) {
                    return new JsonModel(array('status_code' => 202, 'emailAleardyHave' => 'User with this email already exist'));
                } else {
                    if (!$data['agreeterms']) {
                        return new JsonModel(array('status_code' => 402, 'agreement' => 'You must agree to the terms of use.'));
                    } else {
                        $data['minder_id'] = $this->generate();
                        $data['hash'] = $this->generate(12);
                        $data['forgot_hash'] = $this->generate(12);
                        $id = $userTable->register($data);
                        //@todo send email

                        $message = new \Zend\Mail\Message();
                        $html = "Hi Dear  " . "<b>" . $data['first_name'] . "  " . $data['last_name'] . "</b><br>" .
                            "Your Minder ID is:  " . $data['minder_id'] . " <br> <br> <br>" .
                            "To verify you MinderWeb account please click on link below" . "<br>" .
                            "http://minderweb.com/app/index.html#/activate/" . $data['hash'] . " <br> <br> <br>" .
                            "If you have received this in error, please delete this email";
                        $bodyPart = new \Zend\Mime\Message();
                        $bodyMessage = new \Zend\Mime\Part($html);
                        $bodyMessage->type = 'text/html';

                        $bodyPart->setParts(array($bodyMessage));

                        $message->addTo($data['email'])
                            ->addFrom('info@minderweb.com')
                            ->setSubject('Please verify your MinderWeb account')
                            ->setBody($bodyPart);

                        $transport = new SendmailTransport();
                        $transport->send($message);
                        return new JsonModel(array('status_code' => 200, 'message' => ' Please activate account from email and then login with MinderWeb ID  ' . $data['minder_id'], 'minderId' => $data['minder_id']));
                    }
                }

            } else {
                return new JsonModel(array('status_code' => 202, 'message' => $filter->getMessages(), 'request_data' => $data));
            }
        }
    }

    /**
     * Activate user action
     *
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    
   
    
    
    public function update($id, $data)
    {
        $userTable = $this->getUserTable();
        $userTable->update($data, $id);
        return new JsonModel(array('status_code' => 200, 'message' => 'successful activation'));
    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        $userTable = $this->getUserTable();
        $userTable->delete($id);

        $userDeviceTable = $this->getUserDeviceTable();
        $userDeviceTable->deleteByUserId($id);


        return new JsonModel(array('status_code' => 200, 'message' => 'device deleted'));
    }

    public function generate($length = 8)
    {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnm', false);
    }
}
