<?php

/**
 * Class ForgotController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;


class ForgotController extends AbstractRestfulController
{

    /**
     * Get User Service
     * 
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
      //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Logout action
     * 
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        //return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     * 
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Singup action
     * 
     * 
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {
        $user = new User();
        $filter = $user->getInputFilterForForgot();
        $filter->setData($data);
        if ($filter->isValid())
        {
            $userTable=$this->getUserTable();
            $user=$userTable->fetchByEmail($data['email']);
            if(!$user){
                return new JsonModel(array('status_code' => 400, 'message' => 'User does not exist!'));
            } else {
                $forgot_hash= $user->getForgotHash();
                $message = new Message();
                $message->addTo($data['email'])
                    ->addFrom('info@minderweb.com')
                    ->setSubject('Forgot password')
                    ->setBody("You can change your password with this link http://minderweb.com/app/index.html#/reset/".$forgot_hash);
                $transport = new SendmailTransport();
                $transport->send($message);

                return new JsonModel(array('status_code' => 202, 'message' => 'Please check your e-mail'));
            }

            return new JsonModel(array('status_code' => 202, 'message' => 'form is valid'));
        }
        else
        {
            return new JsonModel(array('status_code' => 402, 'message' => 'incorrect e-mail address'));
        }

    }

    /**
     * Activate user action
     * 
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data 
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {
        $result = $this->getService()->activateUser($id, $data['activation_code']);
        return new JsonModel($result);
    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

}
