<?php
namespace User\Controller;


use \Application\Controller\ProtoController;

use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;
use Zend\Math\Rand;
use Zend\Crypt\Password\Bcrypt;

class UserController extends ProtoController
{
    public function index()
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }
    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }
    
    public function getUserDeviceTable()
    {
        return $this->getServiceLocator()->get('UserDevice\Model\UserDeviceTable');
    }
    
    /**
     * Login action
     * @return \Zend\View\Model\JsonModel
     */
    public function loginAction()  //from AuthenticationController
    {
        //Input filters
        $data = $this->getRequest()->getPost();
        $user = new User();
        $filter = $user->getInputFilterForLogin();
        $filter->setData($data);

        if ($filter->isValid()) {
    
            $userTable = $this->getUserTable();
            $user = $userTable->fetchByEmail($data['email']);


            if(!$user){
               
                return new JsonModel(array('status_code' => 400, 'message' => 'User does not exist!'));
               
            } else {
               
                $password = $user->getPassword();
               
                $bcrypt = new Bcrypt();
                if ($bcrypt->verify($data['password'], $password)) {
                    $status = $user->getStatus();
                  
    
                    if ($status==0){
                        return new JsonModel(array('status_code' => 400, 'message' => 'This account is not active'));
                    }
                    if ($status==1){
                        return new JsonModel(array('status_code' => 400, 'message' => 'Please contact with administrator'));
                    }
                    if ($status==2){
                    return new JsonModel(array('status_code' => 200, 'user' => $user));
                    }
                } else {
                    return new JsonModel(array('status_code' => 400, 'message' => 'password is incorrect'));
                }
                return new JsonModel(array('status_code' => 400, 'message' => 'user'));
            }

        } else {

            return new JsonModel(array('status_code' => 202, 'filtermessage' => $filter->getMessages()));
        }
    }
    /**
     * Singup action
     * @return \Zend\View\Model\JsonModel
     */
    public function singupAction() //from UserController
    {
        $data = $this->getRequest()->getPost();
        if (array_key_exists('page', $data)) {
            $args = array();
    
            $users = $this->getUserTable()->getForGrid($args, $data['status'], $data['search'], array("orderType" => $data['orderType'], "orderField" => $data['orderField']), $data['page'], $data['count']);
            $rowsCount = $this->getUserTable()->getCountForGrid($args, $data['status'], $data['search']);
            return new JsonModel(array(
                            'users' => $users,
                            'quantity' => $rowsCount,
            ));
        } else {
            require_once(__DIR__ . "/../../../../Application/src/Application/Lib/recaptchalib.php");
    
            $privatekey = "6LfNUPgSAAAAALwspwBQV4S69S51UPE_CvcurdZ0"; //live
            //        $privatekey = "6LfOUPgSAAAAAEs3EBRp_xRg69Dv2LZMdmPU-8V_"; //local
            $inputJSON = file_get_contents('php://input');
            $input = json_decode( $inputJSON, TRUE );
    
            // If request not from mobile app
            if( $input['captcha']["challenge"] ){
                $resp = recaptcha_check_answer($privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $input['captcha']["challenge"],
                        $input['captcha']["response"]);
    
                if (!$resp->is_valid) {
                    // What happens when the CAPTCHA was entered incorrectly
                    return new JsonModel(array('status_code' => 402, 'agreement' => 'The reCAPTCHA wasn\'t entered correctly. Go back and try it again.'));
                }
            }
    
            $user = new User();
            $filter = $user->getInputFilterForSingup();
            $filter->setData($data);
            if ($filter->isValid()) {
    
                $userTable = $this->getUserTable();
                $currentUser = $userTable->fetchByEmail($data['email']);
                if ($currentUser != false) {
                    return new JsonModel(array('status_code' => 202, 'emailAleardyHave' => 'User with this email already exist'));
                } else {
                    if (!$data['agreeterms']) {
                        return new JsonModel(array('status_code' => 402, 'agreement' => 'You must agree to the terms of use.'));
                    } else {
                        $data['minder_id'] = $this->generate();
                        $data['hash'] = $this->generate(12);
                        $data['forgot_hash'] = $this->generate(12);
                        $id = $userTable->register($data);
                        //@todo send email
    
                        $message = new \Zend\Mail\Message();
                        $html = "Hi Dear  " . "<b>" . $data['first_name'] . "  " . $data['last_name'] . "</b><br>" .
                                "Your Minder ID is:  " . $data['minder_id'] . " <br> <br> <br>" .
                                "To verify you MinderWeb account please click on link below" . "<br>" .
                                "http://minderweb.com/app/index.html#/activate/" . $data['hash'] . " <br> <br> <br>" .
                                "If you have received this in error, please delete this email";
                        $bodyPart = new \Zend\Mime\Message();
                        $bodyMessage = new \Zend\Mime\Part($html);
                        $bodyMessage->type = 'text/html';
    
                        $bodyPart->setParts(array($bodyMessage));
    
                        $message->addTo($data['email'])
                        ->addFrom('info@minderweb.com')
                        ->setSubject('Please verify your MinderWeb account')
                        ->setBody($bodyPart);
    
                        $transport = new SendmailTransport();
                        $transport->send($message);
                        return new JsonModel(array('status_code' => 200, 'message' => ' Please activate account from email and then login with MinderWeb ID  ' . $data['minder_id'], 'minderId' => $data['minder_id']));
                    }
                }
    
            } else {
                return new JsonModel(array('status_code' => 202, 'message' => $filter->getMessages(), 'request_data' => $data));
            }
        }
    }
    
    /**
     * Singup action in "contact us" page
     *
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    
    public function contactAction() //from ContactController
    {
        $data = $this->getRequest()->getPost();
        require_once(__DIR__ . "/../../../../Application/src/Application/Lib/recaptchalib.php");
    
        $privatekey = "6LfNUPgSAAAAALwspwBQV4S69S51UPE_CvcurdZ0"; //live
        //        $privatekey = "6LfOUPgSAAAAAEs3EBRp_xRg69Dv2LZMdmPU-8V_"; //local
        $inputJSON = file_get_contents('php://input');
        $input = json_decode( $inputJSON, TRUE );
        $resp = recaptcha_check_answer($privatekey,
                $_SERVER["REMOTE_ADDR"],
                $input['captcha']["challenge"],
                $input['captcha']["response"]);
    
        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            return new JsonModel(array('status_code' => 402, 'message' => array("captcha" => array("wrong" => 'The reCAPTCHA wasn\'t entered correctly. Try it again.'))));
        }
    
        $user = new User();
        $filter = $user->getInputFilterForContact();
        $filter->setData($data);
        if ($filter->isValid()) {
            switch ($data['contact_type']) {
                case 1:
                    $contact_type = "Yes, by SMS and email";
                    break;
                case 2:
                    $contact_type = "Yes, by SMS only";
                    break;
                case 3:
                    $contact_type = "Yes, by email only";
                    break;
                case 4:
                    $contact_type = "No, not interested";
                    break;
    
            }
            $message = new \Zend\Mail\Message();
            $html = "<b>" . "Your Name: " . "</b>" . $data['name'] . "<br>" .
                    "<b>" . "Your Email: " . "</b>" . $data['email'] . "<br>" .
                    "<b>" . "Your Contact No: " . "</b>" . $data['contact_no'] . "<br>" .
                    "<b>" . "Do you wish to stay in touch with us? " . "</b>" . $contact_type . "<br>" .
                    "<b>" . "Your enquiry details: " . "</b>" . $data['message'];
            $bodyPart = new \Zend\Mime\Message();
            $bodyMessage = new \Zend\Mime\Part($html);
            $bodyMessage->type = 'text/html';
    
            $bodyPart->setParts(array($bodyMessage));
    
            $message->addTo(array('Project-minder@tech-natives.com', $data['email']))
            ->addFrom('Project-minder@tech-natives.com')
            ->setSubject('Contact with  ' . $data['name'])
            ->setBody($bodyPart);
    
    
            $transport = new SendmailTransport();
            $transport->send($message);
    
            return new JsonModel(array('status_code' => 202, 'successfully' => 'Your email has been sent successfully'));
        } else {
    
            return new JsonModel(array('status_code' => 402, 'message' => $filter->getMessages(), 'request_data' => $data));
        }
    }
    
    /**
     * Forgot action
     *
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    
    public function forgotAction() //from ForgotController
    {
        $data = $this->getRequest()->getPost();
        $user = new User();
        $filter = $user->getInputFilterForForgot();
        $filter->setData($data);
        if ($filter->isValid())
        {
            $userTable=$this->getUserTable();
            $user=$userTable->fetchByEmail($data['email']);
            if(!$user){
                return new JsonModel(array('status_code' => 400, 'message' => 'User does not exist!'));
            } else {
                $forgot_hash= $user->getForgotHash();
                $message = new Message();
                $message->addTo($data['email'])
                ->addFrom('info@minderweb.com')
                ->setSubject('Forgot password')
                ->setBody("You can change your password with this link http://minderweb.com/app/index.html#/reset/".$forgot_hash);
                $transport = new SendmailTransport();
                $transport->send($message);
    
                return new JsonModel(array('status_code' => 202, 'message' => 'Please check your e-mail'));
            }
    
            return new JsonModel(array('status_code' => 202, 'message' => 'form is valid'));
        }
        else
        {
            return new JsonModel(array('status_code' => 402, 'message' => 'incorrect e-mail address'));
        }
    
    }
    /**
     * get all users
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function getAllUsersAction() //from UserController
    {
        $userTable = $this->getUserTable();
        $users = $userTable->selectMinderId();
        $count = $userTable->getCount();
        $data = array();
        $allUsers = array();
    
        foreach ($users as $value) {
    
            if ($value['status'] == 2) {
                $data[] = $value;
            }
            $value['of_devices'] = 0;
            $user_id = $value['id'];
            foreach ($count as $key => $val) {
                if ($user_id == $key) {
                    $value['of_devices'] = $val;
                }
            }
    
            $allUsers[] = $value;
    
        }
        return new JsonModel(array('data' => $data, 'allUsers' => $allUsers));
    }
    
    /**
     * Activate user action
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    
    public function activateAction($id) //from UserController
    {
        $data = $this->getRequest()->getPost();
        $userTable = $this->getUserTable();
        $userTable->update($data, $id);
        return new JsonModel(array('status_code' => 200, 'message' => 'successful activation'));
    }
    
    /**
     * get user detail
     *
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function userDetailAction()
    {
        $data = $this->getRequest()->getPost();
        $id = $data['user_id'];
        $userTable=$this->getUserTable();
        $user=$userTable->fetchById($id);
    
        return new JsonModel(array('user'=>$user));
    
    }
    
    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function deleteAction($id) //from UserController
    {
        $userTable = $this->getUserTable();
        $userTable->delete($id);
    
        $userDeviceTable = $this->getUserDeviceTable();
        $userDeviceTable->deleteByUserId($id);
    
    
        return new JsonModel(array('status_code' => 200, 'message' => 'device deleted'));
    }
    /**
     * management user action
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function managementAction($id) //from ManagmentController
    {
        $data = $this->getRequest()->getPost();
        $user = new User();
        $filter = $user->getInputFilterForManagement();
        $filter->setData($data);
        if ($filter->isValid()) {
            if($data['password']){
                $bcrypt = new Bcrypt();
                $password = $bcrypt->create($data['password']);
                $data['password']=$password;
                unset($data['confirm_password']);
            }
            else{
                unset($data['password'],$data['confirm_password']);
            }
            $userTable=$this->getUserTable();
            $userTable->update($data,$id);
            return new JsonModel(array('status_code' => 200, 'message' => 'save'));
    
        }
    
        return new JsonModel(array('status_code' => 400, 'message' => $filter->getMessages()));
    }
    
    /**
     * reset action
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function resetAction($id)// from resetController
    {
        $data = $this->getRequest()->getPost();
        $hash=$data['hash'];
        $password=$data['password'];
        $confirm_password=$data['confirm_password'];
        $flo=array();
        $flo['password']=$password;
        $flo['confirm_password']=$confirm_password;
        $user = new User();
        $filter = $user->getInputFilterForReset();
        $filter->setData($flo);
        if ($filter->isValid()){
    
            $result = $this->getUserTable()->fetchByForgotHash($hash);
            if(!$result){
                return new JsonModel(array('status_code'=>400,'done'=>'Invalid reset password URL!'));
            }
            else{
                $bcrypt = new Bcrypt();
                $new_password = $bcrypt->create($password);
                $this->getUserTable()->resetPassword($new_password, $hash);
                return new JsonModel(array('status_code'=>200,'done'=>'password was successfully changed'));
            }
    
            return new JsonModel(array('reset'));
        }
        else{
            return new JsonModel(array('status_code' => 402, 'message' => $filter->getMessages()));
        }
    }
    
    public function generate($length = 8)
    {
        return Rand::getString($length, '123456789qwertyuipasdfghjkzxcvbnm', false);
    }
}
