<?php

/**
 * Class AuthenticationController
 *
 * @package User
 * @copyright: coeus-solutions.de
 * @version 1.0
 * @author Coeus Solutions
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Crypt\Password\Bcrypt;

class AuthenticationController extends AbstractRestfulController
{

    /**
     * Get user table
     * 
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Logout action
     * 
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     * 
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Login action
     * 
     * 
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {

        //Input filters
        $user = new User();
        $filter = $user->getInputFilterForLogin();
        $filter->setData($data);

        if ($filter->isValid()) {

            $userTable=$this->getUserTable();
            $user=$userTable->fetchByEmail($data['email']);
            if(!$user){
                return new JsonModel(array('status_code' => 400, 'message' => 'User does not exist!'));
            } else {
                $password= $user->getPassword();
                $bcrypt = new Bcrypt();

                if ($bcrypt->verify($data['password'], $password)) {
                    $status=$user->getStatus();

                    if ($status==0){
                        return new JsonModel(array('status_code' => 400, 'message' => 'This account is not active'));
                    }
                    if ($status==1){
                        return new JsonModel(array('status_code' => 400, 'message' => 'Please contact with administrator'));
                    }
                    if ($status==2){
                    return new JsonModel(array('status_code' => 200, 'user' => $user));
                    }
                } else {
                    return new JsonModel(array('status_code' => 400, 'message' => 'password is incorrect'));
                }
                return new JsonModel(array('status_code' => 400, 'message' => 'user'));


            }

/*
                $user = new Auth();
                if ($request->getPost('rememberMe')) {
                    $this->getUserSessionStorage()->setRememberMe(1);
                }
                //set storage again
                $this->getAuthService()->setStorage($this->getUserSessionStorage());

                $this->getAuthService()->getStorage()->write($res);
                if (!$lastUrl) {
                    $lastUrl = "/user";
                }
                return $this->redirect()->toUrl($lastUrl);

//*/

        } else {

            return new JsonModel(array('status_code' => 202, 'filtermessage' => $filter->getMessages()));
        }
    }
    /*
     * $bcrypt = new Bcrypt();
            $securePass = $bcrypt->create($data['password']);*/

    /**
     * Activate user action
     * 
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data 
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {
        $result = $this->getService()->activateUser($id, $data['activation_code']);
        return new JsonModel($result);
    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

}
