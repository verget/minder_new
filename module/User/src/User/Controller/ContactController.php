<?php

/**
 * Class UserController
 *
 * @package User
 * @copyright: be-alternative.info
 * @version 1.0
 * @author Be Alternative
 *
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use User\Model\User;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime;


class ContactController extends AbstractRestfulController
{

    /**
     * Get User Service
     *
     * @author Hrayr Shahbazyan
     * @return \User\Model\UserTable
     */
    public function getModel()
    {
        //  return $this->getServiceLocator()->get('Auth\Model\AuthTable');
    }


    public function getUserTable()
    {
        return $this->getServiceLocator()->get('User\Model\UserTable');
    }

    /**
     * Logout action
     *
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function getList()
    {
        //return new JsonModel($this->getService()->logout());
    }

    /**
     * Get id
     *
     * @todo NOT IMPLEMENTED
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @return \Zend\View\Model\JsonModel
     */
    public function get($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

    /**
     * Singup action
     *
     *
     * @author Hrayr Shahbazyan
     * @param array $data the data from REST Client
     * @return \Zend\View\Model\JsonModel
     */
    public function create($data = array())
    {
        require_once(__DIR__ . "/../../../../Application/src/Application/Lib/recaptchalib.php");

        $privatekey = "6LfNUPgSAAAAALwspwBQV4S69S51UPE_CvcurdZ0"; //live
//        $privatekey = "6LfOUPgSAAAAAEs3EBRp_xRg69Dv2LZMdmPU-8V_"; //local
        $inputJSON = file_get_contents('php://input');
        $input = json_decode( $inputJSON, TRUE );
        $resp = recaptcha_check_answer($privatekey,
            $_SERVER["REMOTE_ADDR"],
            $input['captcha']["challenge"],
            $input['captcha']["response"]);

        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            return new JsonModel(array('status_code' => 402, 'message' => array("captcha" => array("wrong" => 'The reCAPTCHA wasn\'t entered correctly. Try it again.'))));
        }

        $user = new User();
        $filter = $user->getInputFilterForContact();
        $filter->setData($data);
        if ($filter->isValid()) {
            switch ($data['contact_type']) {
                case 1:
                    $contact_type = "Yes, by SMS and email";
                    break;
                case 2:
                    $contact_type = "Yes, by SMS only";
                    break;
                case 3:
                    $contact_type = "Yes, by email only";
                    break;
                case 4:
                    $contact_type = "No, not interested";
                    break;

            }
            $message = new \Zend\Mail\Message();
            $html = "<b>" . "Your Name: " . "</b>" . $data['name'] . "<br>" .
                "<b>" . "Your Email: " . "</b>" . $data['email'] . "<br>" .
                "<b>" . "Your Contact No: " . "</b>" . $data['contact_no'] . "<br>" .
                "<b>" . "Do you wish to stay in touch with us? " . "</b>" . $contact_type . "<br>" .
                "<b>" . "Your enquiry details: " . "</b>" . $data['message'];
            $bodyPart = new \Zend\Mime\Message();
            $bodyMessage = new \Zend\Mime\Part($html);
            $bodyMessage->type = 'text/html';

            $bodyPart->setParts(array($bodyMessage));

            $message->addTo(array('Project-minder@tech-natives.com', $data['email']))
                ->addFrom('Project-minder@tech-natives.com')
                ->setSubject('Contact with  ' . $data['name'])
                ->setBody($bodyPart);


            $transport = new SendmailTransport();
            $transport->send($message);

            return new JsonModel(array('status_code' => 202, 'successfully' => 'Your email has been sent successfully'));
        } else {

            return new JsonModel(array('status_code' => 402, 'message' => $filter->getMessages(), 'request_data' => $data));
        }

    }

    /**
     * Activate user action
     *
     * @author Hrayr Shahbazyan
     * @param integer $id
     * @param array $data
     * @return \Zend\View\Model\JsonModel
     */
    public function update($id, $data)
    {
        $result = $this->getService()->activateUser($id, $data['activation_code']);
        return new JsonModel($result);
    }

    /**
     * Delete method
     *
     * @todo NOT IMPLEMENTED
     * @param integer $id
     * @author Hrayr Shahbazyan
     * @return \Zend\View\Model\JsonModel
     */
    public function delete($id)
    {
        return new JsonModel(array('NOT IMPLEMENTED'));
    }

}
