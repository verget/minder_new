'use strict';

/* Controllers */
angular.module('myApp.controllers', ['reCAPTCHA'])
    .controller('MainCtrl', ['$scope', '$cookieStore', '$location', '$route', '$rootScope', function ($scope, $cookieStore, $location, $route, $rootScope) {
        $rootScope.$on("$routeChangeSuccess", function (currentRoute, previousRoute) {
            //Change page title, based on Route information
            $rootScope.title = $route.current.title;
        });
        $("#loaderImg").hide();
        $scope.loggedIn = $cookieStore.get('loggedin');

        if ($scope.loggedIn == "true") {
            $scope.first_name = $cookieStore.get('first_name');
            $scope.last_name = $cookieStore.get('last_name');
            $scope.user_id = $cookieStore.get('user_id');
            $scope.loggedOut = "";
            $scope.$on('$routeChangeStart', function () {
                if ($location.path() == "/login") {
                    $location.path('/');
                }
                if ($location.path() == "/singup") {
                    $location.path('/');
                }
                if ($location.path() == "/forgot") {
                    $location.path('/');
                }
                if ($location.path() == "/tour") {
                    $location.path('/');
                }
                if ($location.path() == "/contact") {
                    $location.path('/');
                }
                if ($location.path() == "/home") {
                    $location.path('/');
                }
            });


            var user_type = $cookieStore.get('type');
            if (user_type == 'admin') {
                $scope.user_type = true;
            } else if (user_type = 'user') {
                $scope.user_type = false;

            }
        } else {
            $scope.loggedOut = "true";
            $scope.$on('$routeChangeStart', function () {
                if ($location.path() == "/home-map") {
                    $location.path('/');
                }
                if ($location.path() == "/groups") {
                    $location.path('/');
                }
                if ($location.path() == "/management") {
                    $location.path('/');
                }
                if ($location.path() == "/devices") {
                    $location.path('/');
                }
            });

        }

        $scope.logout = function () {
            $cookieStore.put('loggedin', '');
            document.location = '/#/';
            location.reload();

        }
    }])
    .controller('ContactCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.contactData = function () {
            if ($scope.contactUsForm.$valid) {
                $http.post('/user/contact', $scope.contact).success(function (data, status, headers, config) {
                    $scope.message = data.message;
                    $scope.successfully = data.successfully;
                    if ($scope.successfully) {
                        $(".wid").val('');
                        alert($scope.successfully);
                    } else {
                        Recaptcha.reload();
                    }
                }).error(function (data, status, headers, config) {

                });
            }
        };
    }])

    .controller('LoginCtrl', ['$scope', '$http', '$location', '$cookieStore', function ($scope, $http, $location, $cookieStore) {

        $scope.loginUser = function () {
            $http.post('/user/login', $scope.user).success(function (data, status, headers, config) {
            	
                $scope.loggedOut = false;
                $scope.loggedIn = true;
                $cookieStore.put('loggedin', 'true');
                $scope.message = data.message;
                $scope.filtermessages = data.filtermessage;
                
                if( parseInt(data.user.id) > 0 )
                    $cookieStore.put('user_id', data.user.id);
                
                if (!$scope.filtermessages && data.status_code == 200) {
                    $cookieStore.put('first_name', data.user.first_name);
                    $cookieStore.put('last_name', data.user.last_name);
                    $cookieStore.put('user_id', data.user.id);
                    $cookieStore.put('type', data.user.type);
                    console.log('all rigth');
                    document.location = '/#/';
                    location.reload();
                }

            }).error(function (data, status, headers, config) {

            });
        };
    }])
    .config(function (reCAPTCHAProvider) {
        // required, please use your own key :)
        reCAPTCHAProvider.setPublicKey('6LfNUPgSAAAAAHkVOkiZVv6o4_1h8fC4rFPTDZAu'); //live
//        reCAPTCHAProvider.setPublicKey('6LfOUPgSAAAAACyYbWOWkO7mAPBhumjnknqPGuJ4'); //local
        // optional
        reCAPTCHAProvider.setOptions({
            theme: 'white'
        });
    })
    .controller('SingupCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location, reCAPTCHA, reCAPTCHAProvider) {
        $scope.singupUser = function () {

            if ($scope.registerForm.$valid) {
                $scope.showdialog = true;
                $http.post('/user/singup', $scope.singup).success(function (data, status, headers, config) {

                    $scope.message = data.message;
                    $scope.regmessage = data.regmessage;
                    $scope.agreement = data.agreement;
                    $scope.emailaleardyhave = data.emailAleardyHave;
                    if (data.status_code == 200) {
                        $location.path('/confirm');
                    } else {
                        Recaptcha.reload();
                    }

                }).error(function (data, status, headers, config) {

                });
            }

        };

    }])
    .controller('ForgotCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.forgotUser = function () {
            $http.post('/user/forgot', $scope.forgot).success(function (data, status, headers, config) {
                $scope.message = data.message;

            }).error(function (data, status, headers, config) {

            });
        };

    }])
    .controller('HomeCtrl', ['$scope', function ($scope) {
        console.log($scope.loggedIn);
        if ($scope.loggedIn != "true") {
            document.location = '/#/login';
        }
    }])

    .controller('ManagementCtrl', ['$scope', '$http', '$cookieStore', function ($scope, $http, $cookieStore) {
        $scope.quantity = '';
        $scope.count = 10;
        $scope.limit = 10;
        $scope.page = 1;
        $scope.statuse = 0;
        $scope.user_id = '';
        $scope.searchText = '';
        $scope.showing = 10;
        $scope.lastPage = 1;
        $scope.orderType = "asc";
        $scope.orderField = "first_name";

        $scope.getRows = function () {
            $scope.data = {};
            $scope.data['page'] = $scope.page;
            $scope.data['count'] = $scope.count;
            $scope.data['status'] = $scope.statuse;
            $scope.data['search'] = $scope.searchText;
            $scope.data['orderField'] = $scope.orderField;
            $scope.data['orderType'] = $scope.orderType;
            $scope.minder_id = 10;


            $http.post('/user/managementAction/', $scope.data).success(function (data, status, headers, config) {
                $scope.allUsers = data.users;
                $scope.quantity = data.quantity;
            });
        }

        var user_type = $cookieStore.get('type');
        if (user_type == 'admin') {
            $scope.user_type = true;
        } else if (user_type = 'user') {
            $scope.user_type = false;
            $scope.getRows();
        }

        $scope.setPage = function (page) {
            if (page == 'first') {
                $scope.page = 1;
            } else if (page == 'last') {
                $scope.page = $scope.lastPage;
            } else {
                if ($scope.page + page > 0 && $scope.page + page <= $scope.lastPage) {
                    $scope.page = $scope.page + page;
                }
            }

            $scope.getRows();
        }

        $scope.selectUser = function () {
            $('#device_name_label').removeClass('errorlabel');
            $scope.userId = this.selectedUser;
            $scope.getRows();
        }
        $scope.order = function (orderType) {
            $scope.orderType = orderType;
            $scope.getRows();
        }

        $scope.getEntriesCountString = function () {
            var offset = (($scope.page - 1) * $scope.count) + 1;
            if ($scope.quantity == 0) {
                offset = 0;
            }
            var finish = ($scope.page) * $scope.count;

            if (finish > $scope.quantity) {
                finish = $scope.quantity;
            }

            // set pagination variable
            $scope.lastPage = $scope.quantity / $scope.count;
            if ($scope.lastPage - Math.floor($scope.lastPage) > 0) {
                $scope.lastPage = Math.floor($scope.lastPage) + 1;
            }

            return "Showing " + offset + " to " + finish + " of " + $scope.quantity + " entries";
        }

        var load = function () {
            $scope.getRows();


        }
        load();

        $scope.activate = function (user_id) {
            $scope.data = {};
            $scope.data['status'] = 2;
            if (user_id) {
                $http.post('/user/managementAction/' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        load();
                    });
            }
        }

        $scope.delete = function (user_id) {
            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.post('/user/deleteAction/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            load();
                        });

                } else {

                }
            }

        }

    }])

    .controller('UsDetailCtrl', ['$scope', '$routeParams', '$http', '$cookieStore', function ($scope, $routeParams, $http, $cookieStore) {
        var user_id = $routeParams.user_id;
        $scope.selectUser = function () {

            $scope.data = {};
            $scope.data['user_id'] = user_id;
            $http.post('/user/userDetailAction/', $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.user = data.user;
                });
        }

        $scope.selectUser()

        $scope.delete = function (user_id) {
            var profile_id = $cookieStore.get('user_id');
            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.post('/user/delete' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            if (profile_id == user_id) {
                                $scope.logout();
                            } else {
                                document.location = '/#/';
                                location.reload();
                            }

                        });

                } else {

                }
            }

        }

        $scope.save = function () {


            $scope.data = {};
            $scope.data['first_name'] = $('#first_name').val();
            $scope.data['last_name'] = $('#last_name').val();
            $scope.data['email'] = $('#email').val();
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();


            if (!$('#password').val()) {
                $http.post('/user/management' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        $scope.message = data.message;
                        if (data.status_code == 200) {
                            document.location = '/#/';
                        }
                    });
            }

            if ($('#password').val()) {
                if (!$('#confirm_password').val()) {
                    $scope.confirm = "Value is required and can't be empty";
                }
                if ($('#confirm_password').val()) {
                    $scope.confirm = "";
                    $http.post('/user/management' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.message = data.message;
                            if (data.status_code == 200) {
                                document.location = '/#/';
                            }
                        });
                }

            }


        }


    }])

    .controller('ProfileCtrl', ['$scope', '$routeParams', '$http', '$cookieStore', function ($scope, $routeParams, $http, $cookieStore) {
        var user_id = $routeParams.user_id;
        $scope.profile = {};
        $scope.selectUser = function () {

            $scope.data = {};
            $scope.data['user_id'] = user_id;
            $http.post('/user/userDetail', $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.user = data.user;
                });
        }

        $scope.selectUser()

        $scope.delete = function (user_id) {

            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.post('/user/delete' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.logout();
                        });

                } else {

                }
            }

        }

        $scope.save = function () {

            $scope.data = {};
            $scope.data['first_name'] = $('#first_name').val();
            $scope.data['last_name'] = $('#last_name').val();
            $scope.data['email'] = $('#email').val();
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();


            if (!$('#password').val()) {
                $http.post('/user/management' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        $scope.message = data.message;

                        if (data.status_code == 200) {
                            $cookieStore.put('first_name', $scope.data['first_name']);
                            $cookieStore.put('last_name', $scope.data['last_name']);
                            document.location = '/#/';
                            location.reload();
                        }
                    });

            }

            if ($('#password').val()) {
                if (!$('#confirm_password').val()) {
                    $scope.confirm = "Value is required and can't be empty";
                }
                if ($('#confirm_password').val()) {
                    $scope.confirm = '';
                    $http.post('/user/management' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.message = data.message;

                            if (data.status_code == 200) {
                                $cookieStore.put('first_name', $scope.data['first_name']);
                                $cookieStore.put('last_name', $scope.data['last_name']);
                                document.location = '/#/';
                                location.reload();
                            }
                        });
                }

            }


        }


    }])

    .controller('ResetCtrl', function ($scope, $http, $location, $routeParams) {


        var hash = $routeParams.hash;

        $scope.resetPass = function () {
            $scope.data = {};
            $scope.data['hash'] = hash;
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();
            $http.post('/user/reset' + 1, $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.message = data.message;
                    $scope.done = data.done;
                    /*if (data.status_code == 200) {
                     setTimeout(function(){
                     window.location = "http://minder.loc/app/index.html#/login";
                     },3000)
                     }*/

                });

        }
        
        
    })
    
        .controller('MapCtrl', ['$scope', '$http', '$cookieStore', function ($scope, $http, $cookieStore) {
        if ($scope.loggedIn != "true") {
            window.location.href = "/#/login";
       }
        var tm = new Date();
        $http.get('/settimezone?time=' + ( tm.getTimezoneOffset()) / 60 );
        
        
        var user_type = $cookieStore.get('type');
        if (user_type == 'admin') {
            $scope.user_type = true;
            var load = function () {
                $http.post('/user/getAllUsers').success(function (data, status, headers, config) {
                    $scope.users_data = data.data;
                    $scope.showMarkers();
                    $scope.selectedUser = $scope.users_data[0]['id'];
                    $scope.selectUser();
                }).error(function (data, status, headers, config) {

                });

            }
        } else if (user_type = 'user') {
            $scope.user_type = false;
            $scope.getUser = function () {
                user_id = $cookieStore.get('user_id');
                $scope.data = {};
                $scope.data['user_id'] = user_id;
                $scope.selectedUser = user_id;
                $scope.data['version'] = "old";
            }
            
            $scope.getUser();

            var load = function () {
                $http.post('/user/getAllUsers').success(function (data, status, headers, config) {
                    $scope.users_data = data.data;
                    $scope.showMarkers();
                    $scope.getActiveDevice();

                }).error(function (data, status, headers, config) {

                });

            }

        }


        var deviceIds = [];
        var checkedMarkers = [];

        $scope.getCheckedDeviceMarkers = function () {
            if (deviceIds.length != 0) {
                angular.forEach($scope.markers_array, function (value, key) {
                    angular.forEach(deviceIds, function (val, key) {
                        if (value.device_id == val) {
                            checkedMarkers.push(value);
                        }
                    })
                });
            }
            $scope.new_markers = checkedMarkers;
        }

        $scope.check = function () {
            deviceIds = [];
            checkedMarkers = [];
            $('.check_device').each(function () {
                if ($(this).is(":checked")) {
                    deviceIds.push($(this).val())
                }
            });

            if (!deviceIds.length) {
                return false;
            }

            $("#loaderImg").show();

        }

        var geocoder = new google.maps.Geocoder();


        $scope.showMarkers = function () {
            if (!$("#map").length) {
                return false;
            }
            $("#map").height($(window).height() - 105);
            $(".groupsSideBar").height($(window).height() - 300);
            var mapOptions = {
                zoom: 2,
                center: new google.maps.LatLng(-12.0000, 130.0000)
            }

            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

            $scope.markers = [];

            var infoWindow = new google.maps.InfoWindow({ maxHeight: "150px" });

            var createMarker = function (info) {
                var latlng = new google.maps.LatLng(info.lat, info.long);

                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    var Address = false;
                    if (status == google.maps.GeocoderStatus.OK) {
                        console.log(results);
                        if (results[0]) {
                            Address = results[0].formatted_address;
                        }
                    }


                    var pinColor = "FE7569";
                    if (info.last_request_status == "sent") {
                        var pinColor = "BFBDBD";
                    }
                    var pinIcon = new google.maps.MarkerImage(
                            "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                            null, /* size is determined at runtime */
                            null, /* origin is 0,0 */
                            null, /* anchor is bottom center of the scaled image */
                            new google.maps.Size(23, 34)
                        );

                    var marker = new google.maps.Marker({
                        map: $scope.map,
                        position: latlng,
                        title: info.device_name,
                        icon: pinIcon
                    });
                    
                    if( info.last_request_status == 'sent' )
                        marker.content = '<div class="ffinfoWindowContent">' + info.desc + ' (Last Location)</div>';
                    else
                        marker.content = '<div class="ffinfoWindowContent">' + info.desc + ' (Current Location)</div>';


                    google.maps.event.addListener(marker, 'mouseover', function () {
                        var htmlContent = '<h2 style="height: 20px;">' + marker.title + '</h2>';
                        if (Address) {
                            htmlContent += '<p style="height: 20px;">' + Address + '</p>';
                        }
                        htmlContent += marker.content;

                        infoWindow.setContent(htmlContent);
                        infoWindow.open($scope.map, marker);
                    });

                    google.maps.event.addListener(marker, 'mouseout', function () {
                        infoWindow.close();
                    });
                    $scope.markers.push(marker);
                });
            }

            var bounds = new google.maps.LatLngBounds();
            angular.forEach($scope.new_markers, function (value, key) {
                createMarker(value);
                var data = value;
                bounds.extend(new google.maps.LatLng(data.lat, data.long));
            });
            if ($scope.new_markers && $scope.new_markers.length > 0) {
                $scope.map.fitBounds(bounds);
                if ($scope.new_markers.length == 1) {
                    $scope.map.setZoom(16);
                }
            }

            user_id = this.selectedUser;
            $scope.data = {};
            $scope.data['user_id'] = user_id;
            $scope.data['version'] = "old";
        }

        var inactiveDevices = [];
        var activeDevices = [];

        var groups = [];
        var user_id;
        $scope.selectUser = function () {
            $('div.form-group').removeClass('errorlabel');
            user_id = this.selectedUser;
            $scope.selectedUser = user_id;
            $scope.data = {};
            $scope.data.user_id = user_id;
            $scope.data.version = "old";



        }

        load();

        $scope.getInactiveDevice = function () {

            if (user_id) {
                $scope.inactiveDevices = inactiveDevices;
                $scope.groups = [];
                $scope.activeDevices = [];
                $('#activeTabBtn').addClass('active-active');
                $('#inactiveTabBtn').removeClass('active-active');
            }
            else {
                $('div.form-group').addClass('errorlabel');
            }
        }

        $scope.getActiveDevice = function () {
            if (user_id) {
                $scope.activeDevices = activeDevices;
                $scope.groups = groups;
                $scope.inactiveDevices = [];

                $('#inactiveTabBtn').addClass('active-active');
                $('#activeTabBtn').removeClass('active-active');
            } else {
                $('div.form-group').addClass('errorlabel');
            }
        }

        $scope.getCheck = function() {

            deviceIds = [];
            $('.check_device').each(function () {
                if ($(this).is(":checked")) {
                    deviceIds.push($(this).val())
                }
            });
            if (!deviceIds.length) {
                return 0;
            }
            return 1;
        }
    }])

    .controller('ManagementCtrl', ['$scope', '$http', '$cookieStore', function ($scope, $http, $cookieStore) {
        $scope.quantity = '';
        $scope.count = 10;
        $scope.limit = 10;
        $scope.page = 1;
        $scope.statuse = 0;
        $scope.user_id = '';
        $scope.searchText = '';
        $scope.showing = 10;
        $scope.lastPage = 1;
        $scope.orderType = "asc";
        $scope.orderField = "first_name";

        $scope.getRows = function () {
            $scope.data = {};
            $scope.data['page'] = $scope.page;
            $scope.data['count'] = $scope.count;
            $scope.data['status'] = $scope.statuse;
            $scope.data['search'] = $scope.searchText;
            $scope.data['orderField'] = $scope.orderField;
            $scope.data['orderType'] = $scope.orderType;
            $scope.minder_id = 10;


            $http.post('/user', $scope.data).success(function (data, status, headers, config) {
                $scope.allUsers = data.users;
                $scope.quantity = data.quantity;
            });
        }

        var user_type = $cookieStore.get('type');
        if (user_type == 'admin') {
            $scope.user_type = true;
        } else if (user_type = 'user') {
            $scope.user_type = false;
            $scope.getRows();
        }

        $scope.setPage = function (page) {
            if (page == 'first') {
                $scope.page = 1;
            } else if (page == 'last') {
                $scope.page = $scope.lastPage;
            } else {
                if ($scope.page + page > 0 && $scope.page + page <= $scope.lastPage) {
                    $scope.page = $scope.page + page;
                }
            }

            $scope.getRows();
        }

        $scope.selectUser = function () {
            $('#device_name_label').removeClass('errorlabel');
            $scope.userId = this.selectedUser;
            $scope.getRows();
        }
        $scope.order = function (orderType) {
            $scope.orderType = orderType;
            $scope.getRows();
        }

        $scope.getEntriesCountString = function () {
            var offset = (($scope.page - 1) * $scope.count) + 1;
            if ($scope.quantity == 0) {
                offset = 0;
            }
            var finish = ($scope.page) * $scope.count;

            if (finish > $scope.quantity) {
                finish = $scope.quantity;
            }

            // set pagination variable
            $scope.lastPage = $scope.quantity / $scope.count;
            if ($scope.lastPage - Math.floor($scope.lastPage) > 0) {
                $scope.lastPage = Math.floor($scope.lastPage) + 1;
            }

            return "Showing " + offset + " to " + finish + " of " + $scope.quantity + " entries";
        }

        var load = function () {
            $scope.getRows();

        }
        load();

        $scope.activate = function (user_id) {
            $scope.data = {};
            $scope.data['status'] = 2;
            if (user_id) {
                $http.put('/user/' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        load();
                    });
            }
        }

        $scope.delete = function (user_id) {
            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.delete('/user/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            load();
                        });

                } else {

                }
            }

        }

    }])
    .controller('UsDetailCtrl', ['$scope', '$routeParams', '$http', '$cookieStore', function ($scope, $routeParams, $http, $cookieStore) {
        var user_id = $routeParams.user_id;
        $scope.selectUser = function () {

            $scope.data = {};
            $scope.data['user_id'] = user_id;
            $http.post('/user-detail', $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.user = data.user;
                });
        }

        $scope.selectUser()

        $scope.delete = function (user_id) {
            var profile_id = $cookieStore.get('user_id');
            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.delete('/user/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            if (profile_id == user_id) {
                                $scope.logout();
                            } else {
                                document.location = '/#/';
                                location.reload();
                            }

                        });

                } else {

                }
            }

        }

        $scope.save = function () {


            $scope.data = {};
            $scope.data['first_name'] = $('#first_name').val();
            $scope.data['last_name'] = $('#last_name').val();
            $scope.data['email'] = $('#email').val();
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();


            if (!$('#password').val()) {
                $http.put('/management/' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        $scope.message = data.message;
                        if (data.status_code == 200) {
                            document.location = '/#/';
                        }
                    });
            }

            if ($('#password').val()) {
                if (!$('#confirm_password').val()) {
                    $scope.confirm = "Value is required and can't be empty";
                }
                if ($('#confirm_password').val()) {
                    $scope.confirm = "";
                    $http.put('/management/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.message = data.message;
                            if (data.status_code == 200) {
                                document.location = '/#/';
                            }
                        });
                }

            }


        }


    }])
        .controller('ProfileCtrl', ['$scope', '$routeParams', '$http', '$cookieStore', function ($scope, $routeParams, $http, $cookieStore) {
        var user_id = $routeParams.user_id;
        $scope.profile = {};
        $scope.selectUser = function () {

            $scope.data = {};
            $scope.data['user_id'] = user_id;
            $http.post('/user-detail', $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.user = data.user;
                });
        }

        $scope.selectUser()

        $scope.delete = function (user_id) {

            if (user_id) {

                if (confirm("You really want to delete?")) {
                    $http.delete('/user/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.logout();
                        });

                } else {

                }
            }

        }

        $scope.save = function () {

            $scope.data = {};
            $scope.data['first_name'] = $('#first_name').val();
            $scope.data['last_name'] = $('#last_name').val();
            $scope.data['email'] = $('#email').val();
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();


            if (!$('#password').val()) {
                $http.put('/management/' + user_id, $scope.data)
                    .success(function (data, status, headers, config) {
                        $scope.message = data.message;

                        if (data.status_code == 200) {
                            $cookieStore.put('first_name', $scope.data['first_name']);
                            $cookieStore.put('last_name', $scope.data['last_name']);
                            document.location = '/#/';
                            location.reload();
                        }
                    });

            }

            if ($('#password').val()) {
                if (!$('#confirm_password').val()) {
                    $scope.confirm = "Value is required and can't be empty";
                }
                if ($('#confirm_password').val()) {
                    $scope.confirm = '';
                    $http.put('/management/' + user_id, $scope.data)
                        .success(function (data, status, headers, config) {
                            $scope.message = data.message;

                            if (data.status_code == 200) {
                                $cookieStore.put('first_name', $scope.data['first_name']);
                                $cookieStore.put('last_name', $scope.data['last_name']);
                                document.location = '/#/';
                                location.reload();
                            }
                        });
                }

            }


        }


    }])
     .controller('ResetCtrl', function ($scope, $http, $location, $routeParams) {


        var hash = $routeParams.hash;

        $scope.resetPass = function () {
            $scope.data = {};
            $scope.data['hash'] = hash;
            $scope.data['password'] = $('#password').val();
            $scope.data['confirm_password'] = $('#confirm_password').val();
            $http.put('/reset/' + 1, $scope.data)
                .success(function (data, status, headers, config) {
                    $scope.message = data.message;
                    $scope.done = data.done;
                    /*if (data.status_code == 200) {
                     setTimeout(function(){
                     window.location = "http://minder.loc/app/index.html#/login";
                     },3000)
                     }*/

                });

        }
    })
    
     .controller('ActivCtrl', function ($scope, $http, $location, $routeParams) {
        $scope.data = {};
        var hash = $routeParams.hash;
        $scope.data['hash'] = hash;
        var load = function () {
            $http.post('/activate', $scope.data)
                .success(function (data, status, headers, config) {
                    if (data.status_code == 200) {
                        $scope.message = data.message;
                    } else if (data.status_code == 400) {
                        $scope.message = data.message;
                    }
                });
        }

        load();
    });
