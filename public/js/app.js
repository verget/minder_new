'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ngCookies',
  'google-maps',
  'angular-loading-bar'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {templateUrl: '/homei', controller: 'MapCtrl', title: 'Home'});
  $routeProvider.when('/home', {templateUrl: '/homei', controller: 'MainCtrl', title: 'Home'});
  $routeProvider.when('/home-map', {templateUrl: '/mapi', controller: 'MapCtrl', title: 'Home'});
  $routeProvider.when('/confirm', {templateUrl: '/confirm', controller: 'MyCtrl1', title: 'Confirmation'});
  $routeProvider.when('/contact-us', {templateUrl: '/contacti', controller: 'ContactCtrl', title: 'Contact Us'});
  $routeProvider.when('/tour', {templateUrl: '/touri', controller: 'TourCtrl', title: 'Tour'});
  $routeProvider.when('/login', {templateUrl: '/logini', controller: 'LoginCtrl', title: 'Login'});
  $routeProvider.when('/signup', {templateUrl: '/signupi', controller: 'SingupCtrl', title: 'Sign Up'});
  $routeProvider.when('/groups', {templateUrl: '/groupsi', controller: 'GroupsCtrl', title: 'Groups'});
  $routeProvider.when('/devices', {templateUrl: '/devicesi', controller: 'DevicesCtrl', title: 'Devices'});
  $routeProvider.when('/management', {templateUrl: '/managementi', controller: 'ManagementCtrl', title: 'Management'});
  $routeProvider.when('/user-detail/:user_id', {templateUrl: '/usdetaili', controller: 'UsDetailCtrl', title: 'User Details'});
  $routeProvider.when('/profile/:user_id', {templateUrl: '/profilei', controller: 'ProfileCtrl', title: 'My Profile'});
  $routeProvider.when('/forgot', {templateUrl: '/forgoti', controller: 'ForgotCtrl', title: 'Forgot Password'});
  $routeProvider.when('/reset/:hash', {templateUrl: '/reseti', controller: 'ResetCtrl', title: 'Reset Psssword'});
  $routeProvider.when('/activate/:hash', {templateUrl: '/activatei', controller: 'ActivCtrl', title: 'Activation'});
  $routeProvider.when('/terms-and-conditions', {templateUrl: '/termsi', controller: 'TermsCtrl', title: 'Terms And Conditions'});
  $routeProvider.when('/detail/:device_id/:user_id', {templateUrl: '/detaili', controller: 'DetailCtrl', title: 'Device Details'});
  $routeProvider.when('/gr-detail/:group_id/:user_id', {templateUrl: '/grdetaili', controller: 'GrDetailCtrl', title: 'Group Details'});
        $routeProvider.otherwise({redirectTo: '/'});
}]);

